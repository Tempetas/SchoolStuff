import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public final class KyVM {
	public static final int RETURN_SUCCESS = 0, RETURN_FILE_ERROR = 1, RETURN_SYNTAX_ERROR = 2, RETURN_ROM_ERROR = 3;

	public static final int LOGGING_NONE = 0, LOGGING_INFO = 1, LOGGING_DEBUG = 2;

	private final String PREFIX = "[KyVM] ";

	private final int VM_INSTRUCTION_SIZE = 12, VM_MEMORY_SIZE = 256;

	private final byte INSTRUCTION_READ = 0b0000, INSTRUCTION_PRINT = 0b0001, INSTRUCTION_LOAD = 0b0010,
										 INSTRUCTION_LOADI = 0b0011, INSTRUCTION_LOADNUM = 0b0100, INSTRUCTION_STORE = 0b0101,
										 INSTRUCTION_STOREI = 0b0110, INSTRUCTION_ADD = 0b0111, INSTRUCTION_SUB = 0b1000,
										 INSTRUCTION_JUMP = 0b1010, INSTRUCTION_JUMPE = 0b1011, INSTRUCTION_JUMPG = 0b1100,
										 INSTRUCTION_HALT = 0b1101;

	private short ROM[];

	private int loggingLevel = LOGGING_INFO;

	private int execute(Scanner inputStream) {
		//Technically incorrect, should be uninitialized
		short AC = 0;

		short MEMORY[] = new short[VM_MEMORY_SIZE];

		for (int PC = 0; PC < ROM.length; PC++) {
			final int previousPC = PC;

			final short instruction = (short)((ROM[PC] >> 12) & 0x000F);

			final short data = (short)(((ROM[PC] << 5) >> 9) & 0x00FF);

			switch (instruction) {
				case INSTRUCTION_READ:
					print("Input required", LOGGING_INFO);

					AC = inputStream.nextShort();
					break;
				case INSTRUCTION_PRINT:
					System.out.println(Short.toString(AC));
					break;
				case INSTRUCTION_LOAD:
					AC = MEMORY[data];
					break;
				case INSTRUCTION_LOADI:
					AC = MEMORY[MEMORY[data]];
					break;
				case INSTRUCTION_LOADNUM:
					AC = data;
					break;
				case INSTRUCTION_STORE:
					MEMORY[data] = AC;
					break;
				case INSTRUCTION_STOREI:
					MEMORY[MEMORY[data]] = AC;
					break;
				case INSTRUCTION_ADD:
					AC += MEMORY[data];
					break;
				case INSTRUCTION_SUB:
					AC -= MEMORY[data];
					break;
				case INSTRUCTION_JUMP:
					PC = data - 1;
					break;
				case INSTRUCTION_JUMPE:
					if (AC == 0) {
						PC = data - 1;
					}
					break;
				case INSTRUCTION_JUMPG:
					if (AC > 0) {
						PC = data - 1;
					}
					break;
				case INSTRUCTION_HALT:
					print("The machine would've halted on line " + (PC + 1), LOGGING_INFO);

					PC = ROM.length - 1;
					break;
				default:
					printErrorForLine((PC + 1), "Unknown instruction");
					break;
			}

			if (loggingLevel >= LOGGING_DEBUG) {
				String memoryDump = "";

				for (short i = 0; i < MEMORY.length; i++) {
					if (MEMORY[i] != 0) {
						memoryDump += " " + getPaddedBitString(i, 8);
						memoryDump += " -> " + MEMORY[i] + " |";
					}
				}

				final int adjustedPC = ((PC == -1) ? 1 : PC + 1);

				print("State after line " + (previousPC + 1) + ": Last instruction: " + getPaddedBitString(instruction, 4) +
								" " + getPaddedBitString(data, 8) + " | AC: " + AC + " | PC: " + adjustedPC +
								(memoryDump.isEmpty() ? "" : (" | Memory:" + memoryDump)),
							LOGGING_DEBUG);
			}
		}

		return RETURN_SUCCESS;
	}

	public int run() {
		if (ROM == null) {
			printErrorForLine(0, "Nothing to execute, ROM is empty");

			return RETURN_ROM_ERROR;
		}

		int res;

		try {
			Scanner inputStream = new Scanner(System.in);

			res = execute(inputStream);

			inputStream.close();
		} catch (Exception exc) {
			exc.printStackTrace();

			return RETURN_FILE_ERROR;
		}

		return res;
	}

	public int loadROMFromFile(final String filePath) {
		ArrayList<Short> lines = new ArrayList<Short>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath));

			int currentLine = 0;

			short instruction = 0;

			int bit = 0;

			char c;

			while (true) {
				c = (char)reader.read();

				if (c == '0' || c == '1') {
					if (bit >= VM_INSTRUCTION_SIZE) {
						printErrorForLine(currentLine + 1, "Instruction too long");

						return RETURN_SYNTAX_ERROR;
					}

					if (c == '1') {
						instruction |= 1 << (15 - bit);
					}

					bit++;
				} else if (c == '\n' || c == -1 || c == 0xffff) {
					if (bit < VM_INSTRUCTION_SIZE && bit != 4) {
						printErrorForLine(currentLine + 1, "Instruction too short");

						return RETURN_SYNTAX_ERROR;
					}

					lines.add(instruction);

					instruction = 0;

					bit = 0;

					currentLine++;

					if (c != '\n') {
						break;
					}
				}
			}

			reader.close();
		} catch (Exception exc) {
			exc.printStackTrace();

			return RETURN_FILE_ERROR;
		}

		ROM = new short[lines.size()];

		int i = 0;
		for (Short instruction : lines) {
			ROM[i] = instruction;

			i++;
		}

		return RETURN_SUCCESS;
	}

	public void setLoggingLevel(final int level) { loggingLevel = level; }

	private void print(final String str, final int level) {
		if (loggingLevel >= level) {
			System.out.println(PREFIX + str);
		}
	}

	private void printErrorForLine(final int line, final String error) {
		System.err.println(PREFIX + "Error in line " + line + ": " + error);
	}

	private String getPaddedBitString(final int x, final int length) {
		return String.format("%" + length + "s", Integer.toBinaryString(x)).replace(' ', '0');
	}
}